package makananjawa.arif.skripsi.com.makananjawa;

import android.content.pm.ActivityInfo;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ramotion.foldingcell.FoldingCell;

import java.util.ArrayList;
import java.util.List;

import makananjawa.arif.skripsi.com.makananjawa.Adapter.AdapterJajanan;
import makananjawa.arif.skripsi.com.makananjawa.Database.Data_Cache;
import makananjawa.arif.skripsi.com.makananjawa.Model.DataJajanan;
import makananjawa.arif.skripsi.com.makananjawa.Model.SendKategori;
import makananjawa.arif.skripsi.com.makananjawa.Service.APIService;
import makananjawa.arif.skripsi.com.makananjawa.Service.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HalamanList extends AppCompatActivity {

    TextView judul;
    String kategori;
    Toolbar toolbar;

    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView recyclerView;
    List<DataJajanan> myArray = new ArrayList<>();
    AdapterJajanan myAdapter;
    Boolean refreshed = false;
    Data_Cache data_cache;
    String bahasa, detail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_halaman_list);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_arrow_back));

        data_cache = Data_Cache.findById(Data_Cache.class,1L);
        bahasa="";
        String cek = ""+data_cache.bahasa;
        detail = "";
        if(cek.equals("indo"))
        {
            bahasa = "1";
            detail = ""+getResources().getString(R.string.detailIndo);
        }
        else
        {
            bahasa = "2";
            detail = ""+getResources().getString(R.string.detailJawa);
        }

        judul = findViewById(R.id.judul);
        judul.setText(""+getIntent().getStringExtra("judul"));
        kategori = ""+getIntent().getStringExtra("kategori");

        recyclerView = findViewById(R.id.list);
        swipeRefreshLayout = findViewById(R.id.refresh);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshed = true;
                getData();
            }
        });
        swipeRefreshLayout.post(new Runnable() {
             @Override
             public void run() {
                 swipeRefreshLayout.setRefreshing(true);
                 getData();
             }
        });

        /*DataJajanan dataJajanan = new DataJajanan("","","ONDOL ONDOL","","KUDUS","","l;kds ;lsdm;s sdm;asldskfddsfk dslf skdf","","","","","","","");
        myArray.add(dataJajanan);*/
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        myAdapter = new AdapterJajanan(getApplicationContext(),recyclerView,myArray,detail);
        /*recyclerView.setAdapter(myAdapter);*/
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void getData()
    {
        APIService service = ApiClient.getClient().create(APIService.class);
        retrofit2.Call<List<DataJajanan>> searching = service.getData("jajanan_kategori.php?id="+kategori+"&bahasa="+bahasa);
        searching.enqueue(new Callback<List<DataJajanan>>() {
            @Override
            public void onResponse(retrofit2.Call<List<DataJajanan>> call, Response<List<DataJajanan>> response) {
                if(response.isSuccessful()) {
                    Log.e("Data", "" + response.body().toString());
                    try {
                        final List<DataJajanan> data = response.body();
                        if (data.size() == 0) {
                            recyclerView.setVisibility(View.GONE);
                            Toast.makeText(getApplicationContext(), "Tidak Ditemukan", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            if(refreshed == true)
                            {
                                refreshed = false;
                                myArray.clear();
                            }
                            for (int i = 0; i < data.size(); i++)
                            {
                                DataJajanan dataJajanan = new DataJajanan(""+response.body().get(i).get_id_jajananku()
                                        ,""+response.body().get(i).get_kategori()
                                        ,""+response.body().get(i).get_nama()
                                        ,""+response.body().get(i).get_genre()
                                        ,""+response.body().get(i).get_daerah()
                                        ,""+response.body().get(i).get_suku()
                                        ,""+response.body().get(i).get_deskripsi()
                                        ,""+response.body().get(i).get_bahan_dasar()
                                        ,""+response.body().get(i).get_bahan_tambahan()
                                        ,""+response.body().get(i).get_cara_pembuatan()
                                        ,""+response.body().get(i).get_cara_penyajian()
                                        ,""+response.body().get(i).get_cara_makan()
                                        ,""+response.body().get(i).get_fungsi()
                                        ,""+response.body().get(i).get_jumlah_like());
                                myArray.add(dataJajanan);
                            }
                        }
                        recyclerView.setAdapter(myAdapter);
                        myAdapter.notifyDataSetChanged();
                        myAdapter.setLoaded();


                    }
                    catch (Exception e){
                        Toast.makeText(getApplicationContext(), "Tidak Ditemukan", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "ELSE", Toast.LENGTH_SHORT).show();
                }
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(retrofit2.Call<List<DataJajanan>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Cek Koneksi Internet", Toast.LENGTH_SHORT).show();
                //mDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                Log.e("JAJANANKU",""+t);

            }
        });
    }

}
