package makananjawa.arif.skripsi.com.makananjawa;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ramotion.foldingcell.FoldingCell;
import com.squareup.picasso.Picasso;

import makananjawa.arif.skripsi.com.makananjawa.Database.Data_Cache;
import me.biubiubiu.justifytext.library.JustifyTextView;

public class DetailJajanan extends AppCompatActivity {

    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_detail_jajanan);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_arrow_back));

        TextView title = findViewById(R.id.judulku);
        title.setText(""+getIntent().getStringExtra("nama"));

        setUpImage();
        setUpBahasa();
        setUpText();


        final FoldingCell fc = (FoldingCell) findViewById(R.id.folding_cell);
        fc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fc.toggle(false);
            }
        });

        final FoldingCell fc2 = (FoldingCell) findViewById(R.id.folding_cell2);
        fc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fc2.toggle(false);
            }
        });

        final FoldingCell fc3 = (FoldingCell) findViewById(R.id.folding_cell3);
        fc3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fc3.toggle(false);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void setUpImage()
    {
        String myId = getIntent().getStringExtra("id_jajananku");
        ImageView gambar = findViewById(R.id.gambar);
        Picasso.with(getApplicationContext())
                .load("http://mesince2016.000webhostapp.com/API/API_JAJANANKU/gambar/" + myId + ".jpg")
                .noPlaceholder()
                .resize(500, 300)
                .centerCrop()
                .into(gambar);
    }
    public void setUpBahasa()
    {
        //KOTAK 1
        TextView judul = findViewById(R.id.judul);
        TextView judulB = findViewById(R.id.judulB);
        TextView judul1 = findViewById(R.id.judul1);
        TextView judul2 = findViewById(R.id.judul2);
        TextView judul3 = findViewById(R.id.judul3);
        TextView judul4 = findViewById(R.id.judul4);

        //KOTAK 2
        TextView deskripsi = findViewById(R.id.deskripsi);
        TextView deskripsiB = findViewById(R.id.deskripsiB);

        //KOTAK 3
        TextView bahanDanCara = findViewById(R.id.bahanDanCara);
        TextView bahanDanCaraB = findViewById(R.id.bahanDanCaraB);
        TextView bahanDanCara1 = findViewById(R.id.bahanDanCara1);
        TextView bahanDanCara2 = findViewById(R.id.bahanDanCara2);
        TextView bahanDanCara3 = findViewById(R.id.bahanDanCara3);
        TextView bahanDanCara4 = findViewById(R.id.bahanDanCara4);
        TextView bahanDanCara5 = findViewById(R.id.bahanDanCara5);
        TextView bahanDanCara6 = findViewById(R.id.bahanDanCara6);

        Data_Cache data_cache = Data_Cache.findById(Data_Cache.class,1L);
        String cek = ""+data_cache.bahasa;
        if(cek.equals("indo"))
        {
            //KOTAK 1
            judul.setText(""+getResources().getString(R.string.informasiIndo));
            judulB.setText(""+getResources().getString(R.string.informasiIndo));
            judul1.setText(""+getResources().getString(R.string.namaMakananIndo));
            judul2.setText(""+getResources().getString(R.string.genreIndo));
            judul3.setText(""+getResources().getString(R.string.daerahAsalIndo));
            judul4.setText(""+getResources().getString(R.string.sukuBangsaIndo));

            //KOTAK 2
            deskripsi.setText(""+getResources().getString(R.string.deskripsiIndo));
            deskripsiB.setText(""+getResources().getString(R.string.deskripsiIndo));

            //KOTAK 3
            bahanDanCara.setText(""+getResources().getString(R.string.bahanDanCaraPembuatanIndo));
            bahanDanCaraB.setText(""+getResources().getString(R.string.bahanDanCaraPembuatanIndo));
            bahanDanCara1.setText(""+getResources().getString(R.string.bahanDasarIndo));
            bahanDanCara2.setText(""+getResources().getString(R.string.bahanTambahanIndo));
            bahanDanCara3.setText(""+getResources().getString(R.string.caraPembuatanIndo));
            bahanDanCara4.setText(""+getResources().getString(R.string.caraPenyajianIndo));
            bahanDanCara5.setText(""+getResources().getString(R.string.fungsiJajananIndo));
            bahanDanCara6.setText(""+getResources().getString(R.string.caraMakanIndo));

        }
        else
        {
            //KOTAK 1
            judul.setText(""+getResources().getString(R.string.informasiJawa));
            judulB.setText(""+getResources().getString(R.string.informasiJawa));
            judul1.setText(""+getResources().getString(R.string.namaMakananJawa));
            judul2.setText(""+getResources().getString(R.string.genreJawa));
            judul3.setText(""+getResources().getString(R.string.daerahAsalJawa));
            judul4.setText(""+getResources().getString(R.string.sukuBangsaJawa));

            //KOTAK 2
            deskripsi.setText(""+getResources().getString(R.string.deskripsiJawa));
            deskripsiB.setText(""+getResources().getString(R.string.deskripsiJawa));

            //KOTAK 3
            bahanDanCara.setText(""+getResources().getString(R.string.bahanDanCaraPembuatanJawa));
            bahanDanCaraB.setText(""+getResources().getString(R.string.bahanDanCaraPembuatanJawa));
            bahanDanCara1.setText(""+getResources().getString(R.string.bahanDasarJawa));
            bahanDanCara2.setText(""+getResources().getString(R.string.bahanTambahanJawa));
            bahanDanCara3.setText(""+getResources().getString(R.string.caraPembuatanJawa));
            bahanDanCara4.setText(""+getResources().getString(R.string.caraPenyajianJawa));
            bahanDanCara5.setText(""+getResources().getString(R.string.fungsiJajananJawa));
            bahanDanCara6.setText(""+getResources().getString(R.string.caraMakanJawa));

        }
    }

    public void setUpText()
    {
        //KOTAK 1
        TextView nama = findViewById(R.id.nama);
        TextView genre = findViewById(R.id.genre);
        TextView daerah = findViewById(R.id.daerah);
        TextView suku = findViewById(R.id.suku);
        nama.setText(""+getIntent().getStringExtra("nama"));
        genre.setText(""+getIntent().getStringExtra("genre"));
        daerah.setText(""+getIntent().getStringExtra("daerah"));
        suku.setText(""+getIntent().getStringExtra("suku"));

        //KOTAK 2
        JustifyTextView isi_deksripsi = findViewById(R.id.isi_deskripsi);
        isi_deksripsi.setText(""+getIntent().getStringExtra("deskripsi")+"\n\n");

        //KOTAK 3
        TextView bahanDasar = findViewById(R.id.bahanDasar);
        TextView bahanTambahan = findViewById(R.id.bahanTambahan);
        JustifyTextView caraPembuatan = findViewById(R.id.caraPembuatan);
        JustifyTextView caraPenyajian = findViewById(R.id.caraPenyajian);
        JustifyTextView fungsiJajanan = findViewById(R.id.fungsiJajanan);
        JustifyTextView caraMakan = findViewById(R.id.caraMakan);
        bahanDasar.setText(""+getIntent().getStringExtra("bahan_dasar"));
        bahanTambahan.setText(""+getIntent().getStringExtra("bahan_tambahan"));
        caraPembuatan.setText(""+getIntent().getStringExtra("cara_pembuatan")+"\n\n");
        caraPenyajian.setText(""+getIntent().getStringExtra("cara_penyajian")+"\n\n");
        fungsiJajanan.setText(""+getIntent().getStringExtra("fungsi")+"\n\n\n");
        caraMakan.setText(""+getIntent().getStringExtra("cara_makan"));
    }
}
