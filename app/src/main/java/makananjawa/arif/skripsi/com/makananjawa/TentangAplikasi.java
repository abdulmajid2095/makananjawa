package makananjawa.arif.skripsi.com.makananjawa;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import makananjawa.arif.skripsi.com.makananjawa.Database.Data_Cache;
import me.biubiubiu.justifytext.library.JustifyTextView;

public class TentangAplikasi extends AppCompatActivity {

    Toolbar toolbar;
    JustifyTextView deskripsi,deskripsi2;
    Data_Cache data_cache;
    TextView judul;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_tentang_aplikasi);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_arrow_back));

        judul = findViewById(R.id.judulku);
        deskripsi = findViewById(R.id.deskripsi);
        deskripsi2 = findViewById(R.id.deskripsi2);

        data_cache = Data_Cache.findById(Data_Cache.class,1L);
        String cek = ""+data_cache.bahasa;
        if(cek.equals("indo"))
        {
            judul.setText(""+getResources().getString(R.string.tentangAplikasiIndo));
            deskripsi.setText(""+getResources().getString(R.string.deskripsiAplikasiIndo));
            deskripsi2.setText(""+getResources().getString(R.string.deskripsiAplikasi2Indo));
        }
        else
        {
            judul.setText(""+getResources().getString(R.string.tentangAplikasiJawa));
            deskripsi.setText(""+getResources().getString(R.string.deskripsiAplikasiJawa));
            deskripsi2.setText(""+getResources().getString(R.string.deskripsiAplikasi2Jawa));
        }


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
