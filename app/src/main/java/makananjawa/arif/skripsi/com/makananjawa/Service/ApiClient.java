package makananjawa.arif.skripsi.com.makananjawa.Service;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    public static final String BASE_URL = "http://mesince2016.000webhostapp.com/API/API_JAJANANKU/";
    public static final String BASE_URL_DWNLD = "";
    private static Retrofit retrofit = null;


    private static Context context;

    public static Retrofit getClient(){
        if (retrofit == null){

            Gson gson = new GsonBuilder()
                    .setLenient()

                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getDownload(){
        if (retrofit == null){
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL_DWNLD)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }

        return retrofit;
    }
}
