package makananjawa.arif.skripsi.com.makananjawa.Adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import makananjawa.arif.skripsi.com.makananjawa.DetailJajanan;
import makananjawa.arif.skripsi.com.makananjawa.Model.DataJajanan;
import makananjawa.arif.skripsi.com.makananjawa.R;
import makananjawa.arif.skripsi.com.makananjawa.Utilities.ImageCircleTransform;
import makananjawa.arif.skripsi.com.makananjawa.Utilities.OnLoadMoreListener;
import me.biubiubiu.justifytext.library.JustifyTextView;

public class AdapterJajanan extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private OnLoadMoreListener mOnLoadMoreListener;

    private boolean isLoading;
    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    RecyclerView mRecyclerView;
    List<DataJajanan> myArray;
    Context c;
    String detail;

    public AdapterJajanan(Context c, RecyclerView mRecyclerView, List<DataJajanan> myArray, String detail) {

        this.mRecyclerView = mRecyclerView;
        this.myArray = myArray;
        this.c = c;
        this.detail = detail;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        return myArray.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(c).inflate(R.layout.model_list_jajanan, parent, false);
            return new UserViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(c).inflate(R.layout.progressbar, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof UserViewHolder) {

            final DataJajanan dataJajanan = myArray.get(position);
            UserViewHolder userViewHolder = (UserViewHolder) holder;
            //Toast.makeText(c,""+dataJajanan.get_nama(),Toast.LENGTH_SHORT).show();
            userViewHolder.t_nama.setText(""+dataJajanan.get_nama());
            userViewHolder.t_daerah.setText(""+dataJajanan.get_daerah());
            userViewHolder.t_deskripsi.setText(""+dataJajanan.get_deskripsi());
            userViewHolder.t_detail.setText(""+detail);
            Picasso.with(c)
                    .load("http://mesince2016.000webhostapp.com/API/API_JAJANANKU/gambar/" + dataJajanan.get_id_jajananku() + ".jpg")
                    .noPlaceholder()
                    .resize(300, 300)
                    .centerCrop()
                    .transform(new ImageCircleTransform())
                    .into(userViewHolder.gambar);

            userViewHolder.parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(c, DetailJajanan.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("id_jajananku",""+ dataJajanan.get_id_jajananku());
                    intent.putExtra("kategori",""+ dataJajanan.get_kategori());
                    intent.putExtra("nama",""+ dataJajanan.get_nama());
                    intent.putExtra("genre",""+ dataJajanan.get_genre());
                    intent.putExtra("daerah",""+ dataJajanan.get_daerah());
                    intent.putExtra("suku",""+ dataJajanan.get_suku());
                    intent.putExtra("deskripsi",""+ dataJajanan.get_deskripsi());
                    intent.putExtra("bahan_dasar",""+ dataJajanan.get_bahan_dasar());
                    intent.putExtra("bahan_tambahan",""+ dataJajanan.get_bahan_tambahan());
                    intent.putExtra("cara_pembuatan",""+ dataJajanan.get_cara_pembuatan());
                    intent.putExtra("cara_penyajian",""+ dataJajanan.get_cara_penyajian());
                    intent.putExtra("cara_makan",""+ dataJajanan.get_cara_makan());
                    intent.putExtra("fungsi",""+ dataJajanan.get_fungsi());
                    intent.putExtra("jumlah_like",""+ dataJajanan.get_jumlah_like());
                    c.startActivity(intent);
                }
            });

        }


        else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return myArray == null ? 0 : myArray.size();
    }

    public void setLoaded() {
        isLoading = false;
    }


    public class UserViewHolder extends RecyclerView.ViewHolder {
        public TextView t_nama,t_daerah,t_detail;
        JustifyTextView t_deskripsi;
        public ImageView gambar;
        public LinearLayout parent;

        public UserViewHolder(View view) {
            super(view);
            c = itemView.getContext();
            //Toast.makeText(c,"MASUK ADAPTER",Toast.LENGTH_SHORT).show();
            //Toast.makeText(c,"JUMLAH DATA "+myArray.size(),Toast.LENGTH_SHORT).show();
            t_nama = view.findViewById(R.id.nama);
            t_daerah = view.findViewById(R.id.asal);
            t_deskripsi = view.findViewById(R.id.deskripsi);
            t_detail = view.findViewById(R.id.textDetail);
            gambar = view.findViewById(R.id.gambar);
            parent = view.findViewById(R.id.parent);

        }
    }

    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }



}