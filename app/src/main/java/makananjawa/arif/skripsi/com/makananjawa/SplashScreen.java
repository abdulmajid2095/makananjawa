package makananjawa.arif.skripsi.com.makananjawa;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.orm.SugarContext;

import makananjawa.arif.skripsi.com.makananjawa.Database.Data_Cache;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        SugarContext.init(this);
        Data_Cache data = Data_Cache.findById(Data_Cache.class,1L);
        if((int)data.count(Data_Cache.class, "", null) == 0) {

            Data_Cache data3 = new Data_Cache("indo");
            data3.save();
        }

        Handler handler =  new Handler();
        Runnable myRunnable = new Runnable() {
            public void run() {
                // do something
                finish();
                Intent intent = new Intent(SplashScreen.this,Utama.class);
                startActivity(intent);

            }
        };
        handler.postDelayed(myRunnable,2000);
    }
}
