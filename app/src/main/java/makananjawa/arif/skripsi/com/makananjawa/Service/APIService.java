package makananjawa.arif.skripsi.com.makananjawa.Service;

import java.util.List;

import makananjawa.arif.skripsi.com.makananjawa.Model.DataJajanan;
import makananjawa.arif.skripsi.com.makananjawa.Model.SendKategori;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

public interface APIService {
    @GET
    Call<List<DataJajanan>> getData(@Url String url);

    @GET
    Call<List<DataJajanan>> pencarian(@Url String url);
}