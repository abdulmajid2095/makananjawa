package makananjawa.arif.skripsi.com.makananjawa;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
        import android.os.Handler;
        import android.preference.PreferenceManager;
        import android.support.design.widget.NavigationView;
        import android.support.v4.app.Fragment;
        import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
        import android.support.v4.widget.DrawerLayout;
        import android.support.v7.app.ActionBarDrawerToggle;
        import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
        import android.view.View;
        import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
        import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import makananjawa.arif.skripsi.com.makananjawa.Database.Data_Cache;
import makananjawa.arif.skripsi.com.makananjawa.Utilities.ImageCircleTransform;
import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.DrawableBanner;
import ss.com.bannerslider.views.BannerSlider;

public class Utama extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private static final String SELECTED_ITEM_ID = "SELECTED_ITEM_ID";
    private final Handler mDrawerHandler = new Handler();
    private DrawerLayout mDrawerLayout;
    private int mPrevSelectedId;
    private NavigationView mNavigationView;
    private int mSelectedId;
    private Toolbar mToolbar;
    Boolean open = false;


    BannerSlider bannerSlider;
    List<Banner> banners = new ArrayList<>();
    ImageView logoku;
    CardView kategori1, kategori2, kategori3, kategori4, kategori5, kategori6, kategori7, kategori8;
    ImageView imageScene, sceneHeader, icBack;
    TextView textBahanDasar, textKategori1, textKategori2, textKategori3, textKategori4, textKategori5, textKategori6, textKategori7, textKategori8;
    MenuItem item1,item2,item3;
    LinearLayout gradient1,gradient2,gradient3,gradient4,gradient5,gradient6,gradient7,gradient8;
    int selectedNav = 0;
    Menu menu;
    SearchView searchView;
    TextView sidebarDeskripsi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_utama);

        mDrawerLayout = findViewById(R.id.drawer_layout);
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mNavigationView = findViewById(R.id.navigation_view);
        assert mNavigationView != null;
        mNavigationView.setNavigationItemSelectedListener(this);


        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this,
                mDrawerLayout, mToolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                super.onDrawerSlide(drawerView, 0); // this disables the arrow @ completed state
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, 0); // this disables the animation
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        mDrawerToggle.setDrawerIndicatorEnabled(false); //disable "hamburger to arrow" drawable
        mDrawerToggle.setHomeAsUpIndicator(R.drawable.ic_apps_white); //set your own
        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (open) {
                    mDrawerLayout.closeDrawers();
                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }

            }
        });

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        mSelectedId = mNavigationView.getMenu().getItem(prefs.getInt("default_view", 0)).getItemId();
        mSelectedId = savedInstanceState == null ? mSelectedId : savedInstanceState.getInt(SELECTED_ITEM_ID);
        mPrevSelectedId = mSelectedId;
        mNavigationView.getMenu().findItem(mSelectedId).setChecked(true);

        if (savedInstanceState == null) {
            mDrawerHandler.removeCallbacksAndMessages(null);
            mDrawerHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    navigate(mSelectedId);
                }
            }, 250);

            boolean openDrawer = prefs.getBoolean("open_drawer", false);

            if (openDrawer) {
                mDrawerLayout.openDrawer(GravityCompat.START);
                open = true;
            } else {
                mDrawerLayout.closeDrawers();
                open = false;
            }
        }

        mNavigationView.setItemIconTintList(null);
        menu = mNavigationView.getMenu();
        item1 = menu.getItem(0);
        item2 = menu.getItem(1);
        item3 = menu.getItem(2);

        View header = mNavigationView.getHeaderView(0);

        sidebarDeskripsi = header.findViewById(R.id.sidebarDeskripsi);
        logoku = header.findViewById(R.id.logoku);
        sceneHeader = header.findViewById(R.id.sceneHeader);
        icBack = header.findViewById(R.id.back);
        icBack.setOnClickListener(this);
        Picasso.with(this)
                .load(R.drawable.logo)
                .noPlaceholder()
                .transform(new ImageCircleTransform())
                .resize(300, 300)
                .centerCrop()
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .into(logoku);

        bannerSlider = (BannerSlider) findViewById(R.id.banner_slider1);
        DisplayMetrics dm = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getApplicationContext().getSystemService(WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(dm);
        int widthInDP = Math.round(dm.widthPixels / dm.density);
        int i = widthInDP / 16 * 7;
        int h = i + 30;
        final float scale = getApplicationContext().getResources().getDisplayMetrics().density;
        int pixel1 = (int) (widthInDP * scale + 0.5f);
        int pixel2 = (int) (i * scale + 0.5f);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(pixel1, pixel2);
        bannerSlider.setLayoutParams(layoutParams);
        banners.add(new DrawableBanner(R.drawable.slide1));
        banners.add(new DrawableBanner(R.drawable.slide1));
        banners.add(new DrawableBanner(R.drawable.slide2));
        banners.add(new DrawableBanner(R.drawable.slide3));
        bannerSlider.setBanners(banners);
        //new Handler().postDelayed(new Runnable() { @Override public void run() { bannerSlider.setHideIndicators(true); } }, 100);

        imageScene = findViewById(R.id.imageScene);
        kategori1 = findViewById(R.id.kategori1);
        kategori2 = findViewById(R.id.kategori2);
        kategori3 = findViewById(R.id.kategori3);
        kategori4 = findViewById(R.id.kategori4);
        kategori5 = findViewById(R.id.kategori5);
        kategori6 = findViewById(R.id.kategori6);
        kategori7 = findViewById(R.id.kategori7);
        kategori8 = findViewById(R.id.kategori8);
        kategori1.setOnClickListener(this);
        kategori2.setOnClickListener(this);
        kategori3.setOnClickListener(this);
        kategori4.setOnClickListener(this);
        kategori5.setOnClickListener(this);
        kategori6.setOnClickListener(this);
        kategori7.setOnClickListener(this);
        kategori8.setOnClickListener(this);

        textBahanDasar = findViewById(R.id.text_bahan_dasar);
        textKategori1 = findViewById(R.id.text_kategori1);
        textKategori2 = findViewById(R.id.text_kategori2);
        textKategori3 = findViewById(R.id.text_kategori3);
        textKategori4 = findViewById(R.id.text_kategori4);
        textKategori5 = findViewById(R.id.text_kategori5);
        textKategori6 = findViewById(R.id.text_kategori6);
        textKategori7 = findViewById(R.id.text_kategori7);
        textKategori8 = findViewById(R.id.text_kategori8);

        gradient1 = findViewById(R.id.gradien_scene1);
        gradient2 = findViewById(R.id.gradien_scene2);
        gradient3 = findViewById(R.id.gradien_scene3);
        gradient4 = findViewById(R.id.gradien_scene4);
        gradient5 = findViewById(R.id.gradien_scene5);
        gradient6 = findViewById(R.id.gradien_scene6);
        gradient7 = findViewById(R.id.gradien_scene7);
        gradient8 = findViewById(R.id.gradien_scene8);


        Data_Cache data_cache = Data_Cache.findById(Data_Cache.class, 1L);
        data_cache.bahasa = "indo";
        data_cache.save();

        setUpBahasa();
        setUpTheme();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_search, menu);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setQueryHint("");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String cari = ""+query.toString();
                if (cari.isEmpty())
                {
                    cari = "";

                }
                else
                {
                    cari = ""+query.toString();
                }
                Intent intent = new Intent(Utama.this,Pencarian.class);
                intent.putExtra("cari",""+cari);
                startActivity(intent);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
            case R.id.action_search:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void navigate(final int itemId) {
        final View elevation = findViewById(R.id.elevation);
        Fragment navFragment = null;
        switch (itemId) {
            case R.id.drawer_item_1:
                selectedNav = 0;
                item1.setIcon(R.drawable.ic_language_white);
                item2.setIcon(R.drawable.ic_language_grey);
                item3.setIcon(R.drawable.ic_about_grey);

                SpannableString s = new SpannableString("Bahasa Indonesia");
                s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 0, s.length(), 0);
                item1.setTitle(s);
                SpannableString s2 = new SpannableString("Bahasa Jawa");
                s2.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.grey)), 0, s2.length(), 0);
                item2.setTitle(s2);
                SpannableString s3 = new SpannableString(""+getResources().getString(R.string.tentangAplikasiIndo));
                s3.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.grey)), 0, s3.length(), 0);
                item3.setTitle(s3);

                Data_Cache data_cache = Data_Cache.findById(Data_Cache.class,1L);
                data_cache.bahasa = "indo";
                data_cache.save();
                setUpBahasa();
                break;
            case R.id.drawer_item_2:
                selectedNav = 1;
                item1.setIcon(R.drawable.ic_language_grey);
                item2.setIcon(R.drawable.ic_language_white);
                item3.setIcon(R.drawable.ic_about_grey);

                SpannableString s4 = new SpannableString("Bahasa Indonesia");
                s4.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.grey)), 0, s4.length(), 0);
                item1.setTitle(s4);
                SpannableString s5 = new SpannableString("Bahasa Jawa");
                s5.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 0, s5.length(), 0);
                item2.setTitle(s5);
                SpannableString s6 = new SpannableString(""+getResources().getString(R.string.tentangAplikasiJawa));
                s6.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.grey)), 0, s6.length(), 0);
                item3.setTitle(s6);

                Data_Cache data_cache2 = Data_Cache.findById(Data_Cache.class,1L);
                data_cache2.bahasa = "jawa";
                data_cache2.save();
                setUpBahasa();
                break;
            case R.id.drawer_item_3:
                Intent intent = new Intent(Utama.this,TentangAplikasi.class);
                startActivity(intent);
                break;
        }

        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dp(4));

        if (navFragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            try {
                //transaction.replace(R.id.content_frame, navFragment).commit();

                if (elevation != null) {
                    //params.topMargin = navFragment instanceof AdvancedInstallerFragment ? dp(48) : 0;

                    Animation a = new Animation() {
                        @Override
                        protected void applyTransformation(float interpolatedTime, Transformation t) {
                            elevation.setLayoutParams(params);
                        }
                    };
                    a.setDuration(150);
                    elevation.startAnimation(a);
                }
            } catch (IllegalStateException ignored) {
            }
        }
    }

    public int dp(float value) {
        float density = getApplicationContext().getResources().getDisplayMetrics().density;

        if (value == 0) {
            return 0;
        }
        return (int) Math.ceil(density * value);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        menuItem.setChecked(true);
        mSelectedId = menuItem.getItemId();
        mDrawerHandler.removeCallbacksAndMessages(null);
        mDrawerHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                navigate(mSelectedId);
            }
        }, 250);
        mDrawerLayout.closeDrawers();
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SELECTED_ITEM_ID, mSelectedId);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.kategori1:
                Intent intent1 = new Intent(Utama.this,HalamanList.class);
                intent1.putExtra("judul",""+textKategori1.getText().toString());
                intent1.putExtra("kategori","1");
                startActivity(intent1);
                break;

            case R.id.kategori2:
                Intent intent2 = new Intent(Utama.this,HalamanList.class);
                intent2.putExtra("judul",""+textKategori2.getText().toString());
                intent2.putExtra("kategori","2");
                startActivity(intent2);
                break;

            case R.id.kategori3:
                Intent intent3 = new Intent(Utama.this,HalamanList.class);
                intent3.putExtra("judul",""+textKategori3.getText().toString());
                intent3.putExtra("kategori","3");
                startActivity(intent3);
                break;

            case R.id.kategori4:
                Intent intent4 = new Intent(Utama.this,HalamanList.class);
                intent4.putExtra("judul",""+textKategori4.getText().toString());
                intent4.putExtra("kategori","4");
                startActivity(intent4);
                break;

            case R.id.kategori5:
                Intent intent5 = new Intent(Utama.this,HalamanList.class);
                intent5.putExtra("judul",""+textKategori5.getText().toString());
                intent5.putExtra("kategori","5");
                startActivity(intent5);
                break;

            case R.id.kategori6:
                Intent intent6 = new Intent(Utama.this,HalamanList.class);
                intent6.putExtra("judul",""+textKategori6.getText().toString());
                intent6.putExtra("kategori","6");
                startActivity(intent6);
                break;

            case R.id.kategori7:
                Intent intent7 = new Intent(Utama.this,HalamanList.class);
                intent7.putExtra("judul",""+textKategori7.getText().toString());
                intent7.putExtra("kategori","7");
                startActivity(intent7);
                break;

            case R.id.kategori8:
                Intent intent8 = new Intent(Utama.this,HalamanList.class);
                intent8.putExtra("judul",""+textKategori8.getText().toString());
                intent8.putExtra("kategori","8");
                startActivity(intent8);
                break;

            case R.id.back:
                mDrawerLayout.closeDrawers();
                open = false;
                break;
        }
    }

    public void setUpBahasa()
    {
        Data_Cache data_cache = Data_Cache.findById(Data_Cache.class,1L);
        String cek = ""+data_cache.bahasa;
        if(cek.equals("indo"))
        {
            textBahanDasar.setText(""+getResources().getString(R.string.bahanDasarIndo));
            textKategori1.setText(""+getResources().getString(R.string.kategori1Indo));
            textKategori2.setText(""+getResources().getString(R.string.kategori2Indo));
            textKategori3.setText(""+getResources().getString(R.string.kategori3Indo));
            textKategori4.setText(""+getResources().getString(R.string.kategori4Indo));
            textKategori5.setText(""+getResources().getString(R.string.kategori5Indo));
            textKategori6.setText(""+getResources().getString(R.string.kategori6Indo));
            textKategori7.setText(""+getResources().getString(R.string.kategori7Indo));
            textKategori8.setText(""+getResources().getString(R.string.kategori8Indo));
            sidebarDeskripsi.setText(""+getResources().getString(R.string.sidebarIndo));
            setUpBannerIndo();

        }
        else
        {
            textBahanDasar.setText(""+getResources().getString(R.string.bahanDasarJawa));
            textKategori1.setText(""+getResources().getString(R.string.kategori1Jawa));
            textKategori2.setText(""+getResources().getString(R.string.kategori2Jawa));
            textKategori3.setText(""+getResources().getString(R.string.kategori3Jawa));
            textKategori4.setText(""+getResources().getString(R.string.kategori4Jawa));
            textKategori5.setText(""+getResources().getString(R.string.kategori5Jawa));
            textKategori6.setText(""+getResources().getString(R.string.kategori6Jawa));
            textKategori7.setText(""+getResources().getString(R.string.kategori7Jawa));
            textKategori8.setText(""+getResources().getString(R.string.kategori8Jawa));
            sidebarDeskripsi.setText(""+getResources().getString(R.string.sidebarJawa));
            setUpBannerJawa();
        }
    }


    public void setUpTheme()
    {
        Calendar rightNow = Calendar.getInstance();
        int jam = rightNow.get(Calendar.HOUR_OF_DAY);
        //Toast.makeText(getApplicationContext(),"JAM "+jam,Toast.LENGTH_SHORT).show();
        //SORE
        if(jam > 15 && jam < 19)
        {
            //Toast.makeText(getApplicationContext(),"Masuk Sore",Toast.LENGTH_SHORT).show();
            imageScene.setImageResource(R.drawable.scene3);
            sceneHeader.setImageResource(R.drawable.scene3);
            gradient1.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene3));
            gradient2.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene3));
            gradient3.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene3));
            gradient4.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene3));
            gradient5.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene3));
            gradient6.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene3));
            gradient7.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene3));
            gradient8.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene3));
        }
        //MALAM
        else if(jam > 18 || jam < 5)
        {
            //Toast.makeText(getApplicationContext(),"Masuk Malam",Toast.LENGTH_SHORT).show();
            imageScene.setImageResource(R.drawable.scene4);
            sceneHeader.setImageResource(R.drawable.scene4);
            gradient1.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene4));
            gradient2.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene4));
            gradient3.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene4));
            gradient4.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene4));
            gradient5.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene4));
            gradient6.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene4));
            gradient7.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene4));
            gradient8.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene4));
        }
        //PAGI
        else if(jam > 4 && jam < 8)
        {
            //Toast.makeText(getApplicationContext(),"Masuk Pagi",Toast.LENGTH_SHORT).show();
            imageScene.setImageResource(R.drawable.scene1);
            sceneHeader.setImageResource(R.drawable.scene1);
            gradient1.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene1));
            gradient2.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene1));
            gradient3.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene1));
            gradient4.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene1));
            gradient5.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene1));
            gradient6.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene1));
            gradient7.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene1));
            gradient8.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene1));
        }
        else
        {
            //Toast.makeText(getApplicationContext(),"Masuk siang",Toast.LENGTH_SHORT).show();
            imageScene.setImageResource(R.drawable.scene2);
            sceneHeader.setImageResource(R.drawable.scene2);
            gradient1.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene2));
            gradient2.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene2));
            gradient3.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene2));
            gradient4.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene2));
            gradient5.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene2));
            gradient6.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene2));
            gradient7.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene2));
            gradient8.setBackground(getResources().getDrawable(R.drawable.gradient_menu_scene2));
        }
    }

    public void setUpBannerIndo()
    {
        Calendar rightNow = Calendar.getInstance();
        int jam = rightNow.get(Calendar.HOUR_OF_DAY);
        if(banners.size() > 0)
        {
            banners.remove(0);
        }
        //SORE
        if(jam > 14 && jam < 18)
        {
            banners.add(0,new DrawableBanner(R.drawable.ssore_indo));
        }
        //MALAM
        else if(jam > 17 || jam < 3)
        {
            banners.add(0,new DrawableBanner(R.drawable.smalam_indo));
        }
        //PAGI
        else if(jam > 2 && jam < 8)
        {
            banners.add(0,new DrawableBanner(R.drawable.spagi_indo));
        }
        else
        {
            banners.add(0,new DrawableBanner(R.drawable.ssiang_indo));
        }
        bannerSlider.setBanners(banners);
    }

    public void setUpBannerJawa()
    {
        if(banners.size() > 0)
        {
            banners.remove(0);
        }
        Calendar rightNow = Calendar.getInstance();
        int jam = rightNow.get(Calendar.HOUR_OF_DAY);
        //SORE
        if(jam > 14 && jam < 18)
        {
            banners.add(0,new DrawableBanner(R.drawable.ssore_jawa));
        }
        //MALAM
        else if(jam > 17 || jam < 3)
        {
            banners.add(0,new DrawableBanner(R.drawable.smalam_jawa));
        }
        //PAGI
        else if(jam > 2 && jam < 8)
        {
            banners.add(0,new DrawableBanner(R.drawable.spagi_jawa));
        }
        else
        {
            banners.add(0,new DrawableBanner(R.drawable.ssiang_jawa));
        }
        bannerSlider.setBanners(banners);
    }
}
