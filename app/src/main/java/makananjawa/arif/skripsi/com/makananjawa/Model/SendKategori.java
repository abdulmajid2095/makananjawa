package makananjawa.arif.skripsi.com.makananjawa.Model;


import com.google.gson.annotations.SerializedName;

public class SendKategori {
    @SerializedName("id")
    private String id;

    @SerializedName("bahasa")
    private String bahasa;

    public SendKategori(String id, String bahasa) {

        this.id = id;
        this.bahasa = bahasa;
    }
}
