package makananjawa.arif.skripsi.com.makananjawa.Model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataJajanan {
    @SerializedName("id_jajananku")
    @Expose
    private String id_jajananku;

    @SerializedName("kategori")
    @Expose
    private String kategori;

    @SerializedName("nama")
    @Expose
    private String nama;

    @SerializedName("genre")
    @Expose
    private String genre;

    @SerializedName("daerah")
    @Expose
    private String daerah;

    @SerializedName("suku")
    @Expose
    private String suku;

    @SerializedName("deskripsi")
    @Expose
    private String deskripsi;

    @SerializedName("bahan_dasar")
    @Expose
    private String bahan_dasar;

    @SerializedName("bahan_tambahan")
    @Expose
    private String bahan_tambahan;

    @SerializedName("cara_pembuatan")
    @Expose
    private String cara_pembuatan;

    @SerializedName("cara_penyajian")
    @Expose
    private String cara_penyajian;

    @SerializedName("cara_makan")
    @Expose
    private String cara_makan;

    @SerializedName("fungsi")
    @Expose
    private String fungsi;

    @SerializedName("jumlah_like")
    @Expose
    private String jumlah_like;


    public DataJajanan(String id_jajananku, String kategori, String nama, String genre, String daerah
            , String suku, String deskripsi, String bahan_dasar, String bahan_tambahan, String cara_pembuatan
            , String cara_penyajian, String cara_makan, String fungsi, String jumlah_like) {

        this.id_jajananku = id_jajananku;
        this.kategori = kategori;
        this.nama = nama;
        this.genre = genre;
        this.daerah = daerah;
        this.suku = suku;
        this.deskripsi = deskripsi;
        this.bahan_dasar = bahan_dasar;
        this.bahan_tambahan = bahan_tambahan;
        this.cara_pembuatan = cara_pembuatan;
        this.cara_penyajian = cara_penyajian;
        this.cara_makan = cara_makan;
        this.fungsi = fungsi;
        this.jumlah_like = jumlah_like;
    }

    public String get_id_jajananku() {
        return id_jajananku;
    }

    public String get_kategori() {
        return kategori;
    }

    public String get_nama() {
        return nama;
    }

    public String get_genre() {
        return genre;
    }

    public String get_daerah() {
        return daerah;
    }

    public String get_suku() {
        return suku;
    }

    public String get_deskripsi() {
        return deskripsi;
    }

    public String get_bahan_dasar() {
        return bahan_dasar;
    }

    public String get_bahan_tambahan() {
        return bahan_tambahan;
    }

    public String get_cara_pembuatan() {
        return cara_pembuatan;
    }

    public String get_cara_penyajian() {
        return cara_penyajian;
    }

    public String get_cara_makan() {
        return cara_makan;
    }

    public String get_fungsi() {
        return fungsi;
    }

    public String get_jumlah_like() {
        return jumlah_like;
    }

}
